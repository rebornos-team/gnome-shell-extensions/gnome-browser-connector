# gnome-browser-connector

Native browser connector for integration with extensions.gnome.org

https://wiki.gnome.org/Projects/GnomeShellIntegration

https://gitlab.gnome.org/nE0sIghT/gnome-browser-connector

<br>

How to clone this repository:

```
git clone https://gitlab.com/rebornos-team/gnome-shell-extensions/gnome-browser-connector.git
```

